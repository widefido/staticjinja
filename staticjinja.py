"""
Static page generator using Jinja2:
- Templates (by default) live inside `./templates` and will be compiled in '.'.

"""
import inspect
import os
import re

from jinja2 import Environment, FileSystemLoader

try:
    import easywatch
    EASYWATCH_AVAILBLE = True
except ImportError:
    EASYWATCH_AVAILBLE = False


__version_info__ = ('0', '0', '7')
__version__ = '.'.join(__version_info__)


def build_template(env, template, output_dir=None, **kwargs):
    """
    Compile a template.
    - env should be a Jinja environment variable indicating where to find the
      templates.
    - template_name should be the name of the template as it appears inside
      of `./templates`.
    - kwargs should be a series of key-value pairs. These items will be
      passed to the template to be used as needed.

    """
    head, tail = os.path.split(template.name)
    if output_dir:
        head = os.path.join(output_dir, head)
    if head and not os.path.exists(head):
        os.makedirs(head)
    template.stream(**kwargs).dump(os.path.join(head, tail))


def should_render(filename):
    """
    Check if the file should be rendered.
    - Hidden files will not be rendered.
    - Files prefixed with an underscore are assumed to be partials and will
      not be rendered.

    """
    return not (filename.startswith('_') or filename.startswith("."))


def render_templates(env, contexts=None, filter_func=None, rules=None, output_dir=None):
    """
    Render each template inside of `env`.
    - env should be a Jinja environment object.
    - contexts should be a list of regex-function pairs where the
      function should return a context for that template and the regex,
      if matched against a filename, will cause the context to be used.
    - filter_func should be a function that takes a filename and returns
      a boolean indicating whether or not a template should be rendered.
    - rules are used to override template compilation. The value of rules
      should be a list of `regex`-`function` pairs where `function` takes
      a jinja2 Environment, the filename, and the context and builds the
      template, and `regex` is a regex that if matched against a filename
      will cause `function` to be used instead of the default.

    """
    if contexts is None:
        contexts = []
    if filter_func is None:
        filter_func = should_render
    if rules is None:
        rules = []

    for template_name in env.list_templates(filter_func=filter_func):
        print "Building %s..." % template_name

        template = env.get_template(template_name)

        # get the context
        for regex, context_generator in contexts:
            if re.match(regex, template_name):
                try:
                    context = context_generator(template)
                except TypeError:
                    context = context_generator()
                break
        else:
            context = {}

        # build the template
        for regex, func in rules:
            if re.match(regex, template_name):
                func(env, template, output_dir=output_dir, **context)
                break
        else:
            build_template(env, template, output_dir=output_dir, **context)


def main(searchpath="templates", filter_func=None, contexts=None,
         extensions=None, rules=None, autoreload=True, output_dir=None,
         build_callback=None):
    """
    Render each of the templates and then recompile on any changes.
    - searchpath should be the directory that contains the template.
      Defaults to "templates"
    - filter_func should be a function that takes a filename and returns
      a boolean indicating whether or not a template should be rendered.
      Defaults to ignore any files with '.' or '_' prefixes.
    - contexts should be a map of template names to functions where each
      function should return a context for that template.
    - extensions should be any extensions to add to the Environment.
    - autoreload should be a boolean indicating whether or not to
      automatically recompile templates. Defaults to true.
    """
    if extensions is None:
        extensions = []

    # Get calling module
    mod = inspect.getmodule(inspect.stack()[1][0])
    # Absolute path to project
    project_path = os.path.realpath(os.path.dirname(mod.__file__))
    # Absolute path to templates
    template_path = os.path.join(project_path, searchpath)

    loader = FileSystemLoader(searchpath=searchpath)
    env = Environment(loader=loader,
                      extensions=extensions)

    def build_all():
        try:
            render_templates(env, contexts, filter_func=filter_func, rules=rules, output_dir=output_dir)
        except Exception as e:
            print "Templates could not be built."
            print e
        else:
            print "Templates built."
            if build_callback and callable(build_callback):
                build_callback()

    build_all()

    if autoreload and EASYWATCH_AVAILBLE:
        print "Watching '%s' for changes..." % searchpath
        print "Press Ctrl+C to stop."

        def handler(event_type, src_path):
            if event_type == "modified":
                if src_path.startswith(template_path):
                    build_all()
        easywatch.watch("./" + searchpath, handler)
        
        print


if __name__ == "__main__":
    print "feel free to import this module"

